<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Keval Sanghvi',
            'email' => 'kevalssanghvi@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Dhruvi Sanghvi',
            'email' => 'dhruvisanghvi@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);
    }
}
